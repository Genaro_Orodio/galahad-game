Genaro Orodio 
CS 583
San Diego State University Spring 2015

=================================================================================
INTRODUCTION AND INSPIRATION
=================================================================================
	This game is titled "Galahad." It is heavily influenced by old-school 
retro space shooter games, like "Gradius" or "Xevious." Just as old-school games 
are known to be extremely hard, "Galahad" is just as hard. The game invites 
the player to dodge enemies and bullets at extreme speeds to test your reflexes.
I LOVE GAMES LIKE THIS!

The name "Galahad" is a reference to a knight that was part of the Knights of
the Round Table. I liked the whole mythology and stories surrounding King Arthur
and his knights and decided to draw my inspiration from this to make my 2D Game.
I basically wondered if the Knights really did exist, and what would be their
purpose if they existed in a future setting. Each knight gets his own spaceship
to pilot, and the player takes control of Galahad's ship.
================================================================================= 


=================================================================================
CONTROLS
=================================================================================
IT IS RECOMMENDED THAT YOU USE A GAMEPAD (XBOX 360 CONTROLLER) FOR THIS GAME!
THE GAME IS EXTREMELY FAST AND REQUIRES PRECISE CONTROL TO SUCCEED. 
--------------------
Keyboard

w = move up

s = move down

a = move left

d = move right

j = shoot

enter = pause game
--------------------
Xbox Controller

left thumbstick = control ship

A = shoot

Start = pause game
--------------------
=================================================================================


=================================================================================
RULES
=================================================================================
The game is divided in levels. The levels each have 3 types of enemies- a weak
enemy, a strong enemy, and boss at the end of the level. Points for killing 
the enemies are as follows:
	
	weak enemies = 10 points
	
	strong enemies = 20 points

	boss = 100 points

You have 10 health points and 2 lives at the start of each level. If you lose
all your health you lose a life. Lose all your lives then you lose 200 points and
are asked if you want to continue from the beginning of the level or go back to 
the main title screen. 

When you finish the levels, a final score is presented and you are taken back to
the title screen.

Hints:

1. It is possible to shoot enemy bullets

2. Hold down the shoot button to fire continuously
=================================================================================


=================================================================================
Art Assets
=================================================================================
I utilized Photoshop CS6 to create ALL the assets used in this game. EVERYTHING
YOU SEE IN THE GAME WAS CREATED BY ME- FROM THE EXPLOSIONS TO THE BACKGROUNDS
The only art assets that aren't mine are the fonts and letters. I created banners 
and strings with textcraft.net. This website allows you to create strings and 
text in 8-bit graphic formats. This website was perfect for the type of aesthetic
I was trying to attain with my game.

The most trouble I had with creating the art assets would have to be with
creating the backgrounds for the levels. It was extremely time consuming because
I was creating pixel art. Making pixel art on a 1024x512 pixel canvas was
very fun yet very challenging. Photoshop is really powerful. As a result, my
ships and enemies look great, and the 2D parallax backgrounds look phenomenal.
=================================================================================


=================================================================================
Audio Assets
=================================================================================
I did not create any audio for this game. I took sounds and background music from
various users on opengameart.org. See below credits for details.
=================================================================================


=================================================================================
Challenges and Issues
=================================================================================
This project was extremely fun and enabled me to finally incorporate to fun things
I like to do- draw and some programming. The fun was not easy although.

This was the first time I had ever used C# but I was able to adequately learn a
lot of tricks and methods to programming in C#. That was the most difficult part
of this game making process. Please excuse my code if it does not seem up to 
snuff :[

I also had trouble with the scoring system. I was confused on how I would 
implement the system when I had multiple scenes to switch to and from. How would
I be able to preserve this global score variable? The fix was that I made a game
object that would hold the score through a C# script. The object would then be
transported between levels and not destroyed. I achieved this by calling
DontDestroyOnLoad() in the void Start() function.

The Unity interface also had to take some getting used to. At first I thought it
was extremely confusing and discouraging. I had never made a game before and was
overwhelmed by the complexity of the interface. But I looked up some documentation
and worked through the confusion. The Unity engine is very powerful and very
intuitive to use. I like it a lot. I hope to make another game in my personal time
in a couple of weeks.
=================================================================================


=================================================================================
Bugs
=================================================================================
1. When I pause the game and press shoot a bullet is instantiated. This is not 
   supposed to happen.
2. The health, lives, and score texts in the upper left hand corner resize
   very strangely when switching aspect ratios. They can get really big or really
   small. It is recommended you play in 4:3 aspect ratio, as this was how the 
   game was designed to achieve the retro look. If the 4:3 aspect ratio is still
   funky looking, try switching to a higher 4:3 aspect ratio.
=================================================================================


=================================================================================
Credits
=================================================================================
1. Game was made by Unity Engine Version 4.6.2f1
2. 2D assets and pixel art created using Adobe Photoshop CS6
3. Background music by Jan125 from opengameart.org
4. Sound effects i.e. explosions and space sounds by:
	a. DamagedPanda from opengameart.org
	b. jalastram from opengameart.org
5. Oversized fonts and general text made at website textcraft.net
6. General help with C# programming and using unity
	a. Youtube user quill18creates
	b. Youtube user MikeGeigTV
	c. answers.unity3d.com



