﻿using UnityEngine;
using System.Collections;

public class EnemyShooting : MonoBehaviour {

	public GameObject bullet;
	private float shotCooldown = 0f;
	public float delay = 2.0f;
	
	// enemy has cooldown after every shot
	void Update() {
		shotCooldown -= Time.deltaTime;
		if (shotCooldown <= 0 && transform.position.x < 6) { 
			shotCooldown = delay;
			Instantiate(bullet, transform.position, transform.rotation);
		}
	}
}
