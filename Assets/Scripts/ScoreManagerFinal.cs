﻿using UnityEngine;
using System.Collections;

public class ScoreManagerFinal : MonoBehaviour {

	int score;

	// displays score on continue screen and final end screen
	void OnGUI() {
		GameObject go = GameObject.Find("ScoreNumber");
		Score score = (Score)go.GetComponent(typeof(Score));
		GUI.skin.label.fontSize = 46;
		GUI.skin.label.fontStyle = FontStyle.Bold;
		if (Application.loadedLevelName == "Continue Screen")
			GUI.Label (new Rect(0, 0, 800, 50), "Score: " + score.getScore());
		else
			GUI.Label (new Rect(0, 0, 800, 50), "Final Score: " + score.getScore());
	}
}
