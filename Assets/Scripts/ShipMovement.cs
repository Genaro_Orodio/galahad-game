﻿using UnityEngine;
using System.Collections;

public class ShipMovement : MonoBehaviour {
	
	public float shipSpeed = 10f;
	public float shipBoundary = 1f;


	void Update() {
		// ship movement
		transform.Translate(Vector3.right*Input.GetAxis("Horizontal")*shipSpeed*Time.deltaTime);
		transform.Translate(Vector3.up*Input.GetAxis("Vertical")*shipSpeed*Time.deltaTime);

		// keeps ship within aspect ratio bounds
		Vector3 position = transform.position;
		if(position.y+shipBoundary > Camera.main.orthographicSize) {
			position.y = Camera.main.orthographicSize - shipBoundary;
		}
		if(position.y-shipBoundary < -Camera.main.orthographicSize) {
			position.y = -Camera.main.orthographicSize+shipBoundary;
		}
		float screenRatio = (float)Screen.width / (float)Screen.height;
		float widthOrthographicSize = Camera.main.orthographicSize*screenRatio;
	
		if(position.x+shipBoundary > widthOrthographicSize) {
			position.x = widthOrthographicSize - shipBoundary;
		}
		if(position.x-shipBoundary < -widthOrthographicSize) {
			position.x = -widthOrthographicSize+shipBoundary;
		}
		transform.position = position;
	}

}
