﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {

	int score;

	void Start() {
		DontDestroyOnLoad(gameObject);  // this keeps score persistent between level switches
	}
	
	public void enemyTierOneKilled() {
		score += 10;
	}
	
	public void enemyTierTwoKilled() {
		score += 20;
	}
	
	public void bossKilled() {
		score += 100;
	}
	
	public int getScore() {
		return score;
	}

	public void minusScore() {
		score -= 200;
		if (score <= 0)
			score = 0;
	}
	
	public void setScore(int num) {
		int number = num;
		score = number;
	}
}
