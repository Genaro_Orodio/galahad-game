﻿using UnityEngine;
using System.Collections;

public class BossMovement : MonoBehaviour {

	public float speed = 5f;
	
	// boss will stop in front of player
	void Update () {
		if (transform.position.x > 4)
			transform.Translate(Vector3.left * speed * Time.deltaTime);
	}
}
