﻿using UnityEngine;
using System.Collections;

public class EnemyBulletExpiration : MonoBehaviour {

	public float time = 3.0f;
	
	// bullet destroyed after some time
	void Update () {
		time -= Time.deltaTime;
		if (time <= 0)
			Destroy(gameObject);
	}
}
