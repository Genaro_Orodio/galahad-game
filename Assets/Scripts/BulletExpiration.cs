﻿using UnityEngine;
using System.Collections;

public class BulletExpiration : MonoBehaviour {

	public float time = 1.0f;

	// bullet will expire under certain time
	void Update () {
		time -= Time.deltaTime;
		if (time <= 0)
			Destroy(gameObject);
	}
}
