﻿using UnityEngine;
using System.Collections;

public class ScrollBackground : MonoBehaviour {

	public float speed = 0f;

	// used for scrolling parallax 2D background to initiate scrolling
	void Update () {
		renderer.material.mainTextureOffset = new Vector2(Time.time * speed, 0f);
	}
}
