﻿using UnityEngine;
using System.Collections;

public class ShipShooting : MonoBehaviour {
	
	public AudioClip pew;
	public GameObject bullet;
	private float shotCooldown = 0f;
	public float delay = 0.25f;
	public float volume = 1;

	void Update() {
		shotCooldown -= Time.deltaTime;
		if (Input.GetButton("Shoot") && shotCooldown <= 0) { 
			shotCooldown = delay;
			AudioSource.PlayClipAtPoint(pew, transform.position, volume);
			Instantiate(bullet, transform.position, transform.rotation);
		}
	}
}
