﻿using UnityEngine;
using System.Collections;

public class BulletDamageTrigger : MonoBehaviour {
	
	public GameObject boom;
	public AudioClip bang;
	int health = 1;
	public float volume = 1;

	// bullet will explode on impact and make a sound and sprite
	void OnTriggerEnter2D() {
		health--;
		Instantiate(boom, transform.position, transform.rotation);
		AudioSource.PlayClipAtPoint(bang, transform.position, volume);
	}
	
	void Update() {
		if (health <= 0)
			Explode();
	}
	
	void Explode() {
		Destroy (gameObject);
	}
}
