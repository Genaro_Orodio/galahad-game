﻿using UnityEngine;
using System.Collections;

public class ReloadLevel : MonoBehaviour {

	void Update() {
		if (Input.GetButtonDown("Submit")) {
			Application.LoadLevel(Application.loadedLevel - 1);
		}
		if (Input.GetButtonDown("Cancel")) {
			Application.LoadLevel("Game Over Screen");
		}
	}
}
