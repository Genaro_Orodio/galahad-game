﻿using UnityEngine;
using System.Collections;

public class TinyBangExpiration : MonoBehaviour {
	// expiration timer for tiny explosion sprite

	public float time = 0.25f;

	void Update () {
		time -= Time.deltaTime;
		if (time <= 0)
			Destroy(gameObject);
	}
}
