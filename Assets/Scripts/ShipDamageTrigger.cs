﻿using UnityEngine;
using System.Collections;

public class ShipDamageTrigger : MonoBehaviour {

	public AudioClip explosion, ding;
	public GameObject boom, bang;
	public int health = 5;
	public float dingVolume, explosionVolume = 1;

	// sound and graphic will instantiate
	void OnTriggerEnter2D() {
		health--;
		AudioSource.PlayClipAtPoint(ding, transform.position, dingVolume);
		Instantiate (bang, transform.position, transform.rotation);
	}
	
	void Update() {
		if (health <= 0) {
			AudioSource.PlayClipAtPoint(explosion, transform.position, explosionVolume);
			Explode();
			Instantiate(boom, transform.position, transform.rotation);
		}
	}

	// displays character health on screen and will change color based on level
	void OnGUI() {
		if (Application.loadedLevel == 6)
			GUI.color = Color.yellow;
		else if (Application.loadedLevel == 2)
			GUI.color = Color.red;
		else
			GUI.color = Color.black;
		GUI.Label(new Rect(10, 25, 100, 50), "Health: " + health);
	}

	void Explode() {
		Destroy(gameObject);
	}
}
