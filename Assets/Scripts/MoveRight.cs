﻿using UnityEngine;
using System.Collections;

public class MoveRight : MonoBehaviour {

	public float speed = 500f;

	void Update () {
		transform.Translate(Vector3.right * speed * Time.deltaTime);
	}
}
