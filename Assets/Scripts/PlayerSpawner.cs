﻿using UnityEngine;
using System.Collections;

public class PlayerSpawner : MonoBehaviour {

	public GameObject playerPrefab;
	GameObject playerInstance;
	float respawnTimer, delayTimer;

	public int numberLives = 6;

	void Start() {
		delayTimer = 3;
		SpawnPlayer();
	}

	void SpawnPlayer() {
		numberLives--;
		respawnTimer = 1;
		playerInstance = (GameObject)Instantiate(playerPrefab, transform.position, Quaternion.identity);
	}

	// if player dies and still has lives, will respawn in a few moments
	// if player dies and no lives left, will go to continue screen
	void Update() {
		if (playerInstance == null && numberLives == 0) {
			delayTimer -= Time.deltaTime;
			if (delayTimer <= 0) {
				GameObject go = GameObject.Find("ScoreNumber");
				Score score = (Score)go.GetComponent(typeof(Score));
				score.minusScore();
				Application.LoadLevel(Application.loadedLevel+1);
			}
		}
		else if (playerInstance == null) {
			respawnTimer -= Time.deltaTime;
			if (respawnTimer <= 0) 
				SpawnPlayer();
		}
	}

	// displays lives remaining on screen
	void OnGUI() {
		if (Application.loadedLevel == 6)
			GUI.color = Color.yellow;
		else if (Application.loadedLevel == 2)
			GUI.color = Color.red;
		else
			GUI.color = Color.black;
		GUI.Label (new Rect(10, 0, 200, 50), "Lives Remaining: " + numberLives);
	}
}
