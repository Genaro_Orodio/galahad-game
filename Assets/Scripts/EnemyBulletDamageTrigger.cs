﻿using UnityEngine;
using System.Collections;

public class EnemyBulletDamageTrigger : MonoBehaviour {

	int health = 1;
	
	void OnTriggerEnter2D() {
		health--;
	}

	// bullet destroyed on impacts
	void Update() {
		if (health <= 0)
			Explode();
	}
	
	void Explode() {
		Destroy(gameObject);
	}
}
