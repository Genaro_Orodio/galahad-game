﻿using UnityEngine;
using System.Collections;

public class EnemyDamageTriggerTierTwo : MonoBehaviour {

	public GameObject boom;
	public int health = 2;
	public AudioClip deathSound;
	public float volume = 0.3f;
	
	void OnTriggerEnter2D() {
		health--;
	}

	// if enemy dies 20 points added to score
	void Update() {
		if (health <= 0) {
			AudioSource.PlayClipAtPoint(deathSound, transform.position, volume);
			Instantiate (boom, transform.position, transform.rotation);
			Explode();
			GameObject go = GameObject.Find("ScoreNumber");
			Score score = (Score)go.GetComponent(typeof(Score));
			score.enemyTierTwoKilled();
		}
	}
	
	void Explode() {
		Destroy(gameObject);
	}
}
