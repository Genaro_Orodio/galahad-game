﻿using UnityEngine;
using System.Collections;

public class FloatPattern : MonoBehaviour {

	private float startY= 0f;
	public float duration = .75f;

	void Start() {
		startY = transform.position.y;
	}
	
	// object will move in up and down pattern
	void Update () {
		float newY = startY + (startY + Mathf.Cos (Time.time/duration*4)/2);
		transform.position = new Vector2(transform.position.x, newY);
	}
}
