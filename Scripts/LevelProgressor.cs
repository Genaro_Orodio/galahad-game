﻿using UnityEngine;
using System.Collections;

public class LevelProgressor : MonoBehaviour {

	float delayTimer = 10f;
	public float speed = 5f;

	void Update () {
		// finds the objects with these names 
		// if the objects are not found, "Mission Accomplished!" is shown
		// if the objects are not found, loads next level
		GameObject go = GameObject.Find("avalonboss");
		GameObject jo = GameObject.Find("lostboss");
		GameObject lo = GameObject.Find("spaceboss");
		if (jo == null && go == null && lo == null) {
			delayTimer -= Time.deltaTime;
			if (transform.position.y > 0.5)
				transform.Translate(Vector3.down * speed * Time.deltaTime);
			if (delayTimer <= 0)
				Application.LoadLevel(Application.loadedLevel + 2);
		}
	}
}
