﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	int score;

	// displays score on screen and will change color depending on level
	void OnGUI() {
		GameObject go = GameObject.Find("ScoreNumber");
		Score score = (Score)go.GetComponent(typeof(Score));
		GUI.skin.label.fontSize = 20;
		GUI.skin.label.fontStyle = FontStyle.Bold;
		if (Application.loadedLevel == 6)
			GUI.color = Color.yellow;
		else if (Application.loadedLevel == 2)
			GUI.color = Color.red;
		else
			GUI.color = Color.black;
		GUI.Label (new Rect(10, 50, 200, 50), "Score: " + score.getScore());
	}
}
