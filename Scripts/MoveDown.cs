﻿using UnityEngine;
using System.Collections;

public class MoveDown : MonoBehaviour {

	public float speed = 5f;

	void Update () {
		if (transform.position.y > 1.5)
			transform.Translate(Vector3.down * speed * Time.deltaTime);
	}
}
