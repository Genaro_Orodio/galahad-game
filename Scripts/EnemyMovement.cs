﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {

	public float speed = 5f;
	
	// right to left enemy movement
	void Update () {
		if (transform.position.x > -9)
			transform.Translate(Vector3.left * speed * Time.deltaTime);
		else
			Destroy(gameObject);
	}
}
