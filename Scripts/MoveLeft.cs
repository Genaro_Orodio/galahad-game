﻿using UnityEngine;
using System.Collections;

public class MoveLeft : MonoBehaviour {

	public float speed = 250f;
	
	// Update is called once per frame
	void Update () {
		transform.Translate(Vector3.left * speed * Time.deltaTime);
	}
}
