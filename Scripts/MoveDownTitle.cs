﻿using UnityEngine;
using System.Collections;

public class MoveDownTitle : MonoBehaviour {

	public float speed = 5f;
	public float threshhold = 2f;
	
	void Update () {
		if (transform.position.y > threshhold)
			transform.Translate(Vector3.down * speed * Time.deltaTime);
	}
}
