﻿using UnityEngine;
using System.Collections;

public class BossDamageTrigger : MonoBehaviour {

	public AudioClip explosionSound;
	public GameObject boom;
	public int health = 50;
	public float volume = 1;
	
	void OnTriggerEnter2D() {
		health--;
	}

	// if boss dies 100 points are added to the score
	void Update() {
		if (health <= 0) {
			AudioSource.PlayClipAtPoint(explosionSound, transform.position, volume);
			Instantiate (boom, transform.position, transform.rotation);
			Explode();
			GameObject go = GameObject.Find("ScoreNumber");
			Score score = (Score)go.GetComponent(typeof(Score));
			score.bossKilled();
		}
	}
	
	void Explode() {
		Destroy(gameObject);
	}
}
