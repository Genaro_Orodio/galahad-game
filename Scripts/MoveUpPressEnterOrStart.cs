﻿using UnityEngine;
using System.Collections;

public class MoveUpPressEnterOrStart : MonoBehaviour {

	public float speed = 5f;
	public float threshhold = -2;
	
	void Update () {
		if (transform.position.y < threshhold)
			transform.Translate(Vector3.up * speed * Time.deltaTime);
	}
}
