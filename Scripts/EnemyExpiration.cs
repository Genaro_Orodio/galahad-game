﻿using UnityEngine;
using System.Collections;

public class EnemyExpiration : MonoBehaviour {

	public float time = 4.0f;
	
	// enemy will get destroy if past main camera
	void Update () {
		time -= Time.deltaTime;
		if (time <= 0)
			Destroy(gameObject);
	}
}
