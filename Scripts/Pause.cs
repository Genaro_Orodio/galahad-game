﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {

	private bool pauseGame = false;
	public float volume = 0.3f;
	public AudioClip pauseSound;
	public GameObject pauseText;


	// if game paused a pause graphic will appear
	// if game paused and user hits cancel button, game will reset to title screen
	void Update() {
		if (Input.GetButtonDown("Cancel") && pauseGame == true) {
					Time.timeScale = 1;
					Destroy(GameObject.Find("ScoreNumber"));
					Application.LoadLevel("Title Screen");
		}
		if (Input.GetButtonDown("Submit")) {
			pauseGame = ! pauseGame;
			AudioSource.PlayClipAtPoint(pauseSound, transform.position, volume);
			if (pauseGame == true) {
				Time.timeScale = 0;
				pauseGame = true;
				audio.Pause();
				PauseAppear();
			}
			if (pauseGame == false) {
				Time.timeScale = 1;
				pauseGame = false;
				AudioSource.PlayClipAtPoint(pauseSound, transform.position, volume);
				PauseDisappear();  // deletes clone of pause graphic
				audio.Play();
			}
		}
		if (pauseGame == false) {
			Time.timeScale = 1;
			pauseGame = false;
		}

	}

	void PauseDisappear() {
		GameObject obj = GameObject.Find("Paused(Clone)");
		if (obj)
			Destroy (obj.gameObject);
	}
	
	void PauseAppear() {
		Instantiate(pauseText, transform.position + transform.forward, transform.rotation);
	}

}
