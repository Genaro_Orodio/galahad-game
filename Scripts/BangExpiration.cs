﻿using UnityEngine;
using System.Collections;

public class BangExpiration : MonoBehaviour {

	public float time = 3.0f;
	
	// explosion will expire under certain time
	void Update () {
		time -= Time.deltaTime;
		if (time <= 0)
			Destroy(gameObject);
	}
}